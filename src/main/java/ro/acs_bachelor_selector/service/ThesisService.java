package ro.acs_bachelor_selector.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.acs_bachelor_selector.model.Professor;
import ro.acs_bachelor_selector.model.Student;
import ro.acs_bachelor_selector.model.Thesis;
import ro.acs_bachelor_selector.repository.ProfessorRepo;
import ro.acs_bachelor_selector.repository.StudentRepo;
import ro.acs_bachelor_selector.repository.ThesisRepo;
import ro.acs_bachelor_selector.rest.dto.request.SaveThesisRequest;
import ro.acs_bachelor_selector.rest.dto.request.UpdateThesisRequest;
import ro.acs_bachelor_selector.service.exception.ApplicationException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class ThesisService {

    @Autowired
    private ThesisRepo thesisRepo;

    @Autowired
    private ProfessorRepo professorRepo;

    @Autowired
    private StudentRepo studentRepo;


    public Thesis processAddThesis(final SaveThesisRequest request) {
        final Optional<Professor> _prof = professorRepo.findById(request.getProfId());
        if (!_prof.isPresent()) {
            throw new ApplicationException("Professor not found.");
        }

        Thesis t = new Thesis(request.getTitle(), null, _prof.get());
        thesisRepo.save(t);

        return t;
    }


    public Optional<Thesis> find(final Integer id) {
        return thesisRepo.findById(id);
    }


    public List<Thesis> findAll() {
        List<Thesis> result = new ArrayList<>();
        thesisRepo.findAll().forEach(result::add);

        return result;
    }


    public void processUpdateThesis(final int id, final UpdateThesisRequest request) {
        final Optional<Student> _student = studentRepo.findById(request.getNewStudent());
        if (!_student.isPresent()) {
            throw new ApplicationException("Student not found.");
        }

        final Optional<Thesis> _thesis = thesisRepo.findById(id);
        if (!_thesis.isPresent()) {
            throw new ApplicationException("Thesis not found.");
        }

        final Thesis thesis = _thesis.get();
        final Student student = _student.get();

        thesis.setTaken(true);
        thesis.setStud(student);
        thesisRepo.save(thesis);
    }
}
