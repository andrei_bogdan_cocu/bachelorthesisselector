package ro.acs_bachelor_selector.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.acs_bachelor_selector.model.Account;
import ro.acs_bachelor_selector.model.Professor;
import ro.acs_bachelor_selector.model.Student;
import ro.acs_bachelor_selector.repository.AccountRepo;
import ro.acs_bachelor_selector.repository.ProfessorRepo;
import ro.acs_bachelor_selector.repository.StudentRepo;
import ro.acs_bachelor_selector.rest.dto.request.LoginPersonRequest;
import ro.acs_bachelor_selector.service.exception.AccountNotFoundException;
import ro.acs_bachelor_selector.service.exception.PersonNotFoundException;

import java.util.Optional;


@Service
public class LoginService {

    @Autowired
    private AccountRepo accountRepo;

    @Autowired
    private StudentRepo studentRepo;

    @Autowired
    private ProfessorRepo professorRepo;


    public Student attemptLoginStudent(final LoginPersonRequest request) {
        final Optional<Account> _account = accountRepo.findAccountByUsernameAndPassword(request.getUsername(), request.getPassword());

        final Optional<Student> student = _account.map(account -> studentRepo.findByAccount_Id(account.getId()))
                .orElseThrow(() -> new AccountNotFoundException("Username/password not found."));

        return student.orElseThrow(() -> new PersonNotFoundException());
    }


    public Professor attemptLoginProfessor(final LoginPersonRequest request) {
        final Optional<Account> _account = accountRepo.findAccountByUsernameAndPassword(request.getUsername(), request.getPassword());

        final Optional<Professor> professor = _account.map(account -> professorRepo.findByAccount_Id(account.getId()))
                .orElseThrow(() -> new AccountNotFoundException("Username/password not found."));

        return professor.orElseThrow(() -> new PersonNotFoundException());
    }


}
