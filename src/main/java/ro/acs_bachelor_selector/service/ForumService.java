package ro.acs_bachelor_selector.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.acs_bachelor_selector.model.Forum;
import ro.acs_bachelor_selector.model.Thesis;
import ro.acs_bachelor_selector.repository.ForumRepo;
import ro.acs_bachelor_selector.repository.ThesisRepo;
import ro.acs_bachelor_selector.rest.dto.request.SaveForumRequest;
import ro.acs_bachelor_selector.service.exception.ApplicationException;

import java.util.Optional;


@Service
public class ForumService {

    @Autowired
    private ThesisRepo thesisRepo;

    @Autowired
    private ForumRepo forumRepo;


    public Forum performSaveForum(final SaveForumRequest request) {
        final Optional<Thesis> _thesis = thesisRepo.findById(request.getThesisId());
        if (!_thesis.isPresent()) {
            throw new ApplicationException("Please create a thesis first");
        }

        final Thesis thesis = _thesis.get();
        Forum forum = new Forum(request.getName(), request.getText(), thesis.getProf(), thesis.getStud());

        forumRepo.save(forum);
        thesis.setForum(forum);

        thesisRepo.save(thesis);

        return forum;
    }


    public Optional<Forum> find(final Integer id) {
        return forumRepo.findById(id);
    }

    public Iterable<Forum> findAll() {
        return forumRepo.findAll();
    }
}
