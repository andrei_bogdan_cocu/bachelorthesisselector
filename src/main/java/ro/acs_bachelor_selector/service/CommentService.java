package ro.acs_bachelor_selector.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ro.acs_bachelor_selector.model.Comment;
import ro.acs_bachelor_selector.model.Forum;
import ro.acs_bachelor_selector.model.Professor;
import ro.acs_bachelor_selector.model.Student;
import ro.acs_bachelor_selector.repository.CommentRepo;
import ro.acs_bachelor_selector.repository.ForumRepo;
import ro.acs_bachelor_selector.repository.ProfessorRepo;
import ro.acs_bachelor_selector.repository.StudentRepo;
import ro.acs_bachelor_selector.rest.dto.request.SaveCommentRequest;
import ro.acs_bachelor_selector.service.exception.ApplicationException;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.Optional;


@Service
public class CommentService {

    @Autowired
    private CommentRepo commentRepo;

    @Autowired
    private ForumRepo forumRepo;

    @Autowired
    private StudentRepo studentRepo;

    @Autowired
    private ProfessorRepo professorRepo;


    public Optional<Comment> find(final Integer id) {
        return commentRepo.findById(id);
    }

    public Comment processSaveCommentRequest(final SaveCommentRequest request) {
        final Optional<Forum> _forum = forumRepo.findById(request.getForumId());
        if (!_forum.isPresent()) {
            throw new ApplicationException("A Forum must be created first.");
        }

        Forum forum = _forum.get();

        if (request.getStudId() != null) {
            final Optional<Student> _student = studentRepo.findById(request.getStudId());
            if (!_student.isPresent()) {
                throw new ApplicationException("Student not found.");
            }

            if (!forum.getParticipants().contains(_student.get())) {
                throw new ApplicationException("Student not allowed to comment");
            }

            Comment comment = new Comment(request.getText(), LocalDateTime.now(), _student.get());
            commentRepo.save(comment);

            _forum.get().addComment(comment);
            forumRepo.save(_forum.get());

            return comment;

        } else if (request.getProfId() != null) {
            final Optional<Professor> _prof = professorRepo.findById(request.getProfId());
            if (!_prof.isPresent()) {
                throw new ApplicationException("Professor not found.");
            }

            if (!forum.getProfessor().equals(_prof.get())) {
                throw new ApplicationException("Professor not allowed to comment");
            }

            Comment comment = new Comment(request.getText(), LocalDateTime.now(), _prof.get());
            commentRepo.save(comment);

            _forum.get().addComment(comment);
            forumRepo.save(_forum.get());
            return comment;
        }
        return null;
    }


    public Comment attachFile(final Integer commentId, final MultipartFile file) {
        final Path path = Paths.get("C:/bachelor/files/" + file.getOriginalFilename());
        try {
            file.transferTo(path);
        } catch (IOException e) {
            throw new ApplicationException("Could not save file.");
        }

        final Optional<Comment> _comment = commentRepo.findById(commentId);
        if (_comment.isEmpty()) {
            throw new ApplicationException("Comment not found");
        }

        final Comment comment = _comment.get();

        comment.setAttachment(path.toFile().getAbsolutePath());
        comment.setAttachmentFileType(file.getContentType());
        commentRepo.save(comment);
        return comment;
    }


    public Iterable<Comment> findAll() {
        return commentRepo.findAll();
    }


    public byte[] getFile(final String filename) {
        try {
            ResourceLoader resourceLoader = new FileSystemResourceLoader();
            Resource resource = resourceLoader.getResource(filename);
            return resource.getInputStream().readAllBytes();
        } catch (Exception e) {
            throw new ApplicationException("Can't find file: " + filename);
        }
    }


}