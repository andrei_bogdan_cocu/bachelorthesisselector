package ro.acs_bachelor_selector.service.exception;

public class ApplicationException extends RuntimeException {

    public ApplicationException() {
    }


    public ApplicationException(final String message) {
        super(message);
    }


    public ApplicationException(final String message, final Throwable cause) {
        super(message, cause);
    }


    public ApplicationException(final Throwable cause) {
        super(cause);
    }


    public ApplicationException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
