package ro.acs_bachelor_selector.service.exception;

public class AccountNotFoundException extends ApplicationException {

    public AccountNotFoundException() {
    }


    public AccountNotFoundException(final String message) {
        super(message);
    }


    public AccountNotFoundException(final String message, final Throwable cause) {
        super(message, cause);
    }


    public AccountNotFoundException(final Throwable cause) {
        super(cause);
    }


    public AccountNotFoundException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
