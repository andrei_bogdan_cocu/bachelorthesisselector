package ro.acs_bachelor_selector.service.exception;

public class PersonNotFoundException extends ApplicationException {
    public PersonNotFoundException() {
    }


    public PersonNotFoundException(final String message) {
        super(message);
    }


    public PersonNotFoundException(final String message, final Throwable cause) {
        super(message, cause);
    }


    public PersonNotFoundException(final Throwable cause) {
        super(cause);
    }


    public PersonNotFoundException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
