package ro.acs_bachelor_selector.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.acs_bachelor_selector.model.Student;
import ro.acs_bachelor_selector.repository.AccountRepo;
import ro.acs_bachelor_selector.repository.StudentRepo;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class StudentService {

    @Autowired
    private StudentRepo studentRepo;

    @Autowired
    private AccountRepo accountRepo;


    public Optional<Student> find(final Integer id) {
        return studentRepo.findById(id);
    }

    public Iterable<Student> findAll() {
        return studentRepo.findAll();
    }
}
