package ro.acs_bachelor_selector.startup;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ro.acs_bachelor_selector.model.*;
import ro.acs_bachelor_selector.repository.*;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Consumer;

@Component
public class DatabaseLoader implements CommandLineRunner {

    private static final Logger LOG = LoggerFactory.getLogger(DatabaseLoader.class);

    @Autowired
    private StudentRepo studentRepo;

    @Autowired
    private ProfessorRepo professorRepo;

    @Autowired
    private ThesisRepo thesisRepo;

    @Autowired
    private ForumRepo forumRepo;

    @Autowired
    private CommentRepo commentRepo;

    @Autowired
    private AccountRepo accountRepo;


    /**
     * Initial database setup so that we don't have to manually create it
     * everytime the application starts.
     *
     * @param args
     */
    @Override
    @Transactional
    public void run(String... args) {
        Account a1 = new Account("adinca", "parola");
        Account a2 = new Account("bcocu", "parola");
        Account a3 = new Account("jdoe", "parola");

        accountRepo.save(a1);
        accountRepo.save(a2);
        accountRepo.save(a3);

        Student s1 = new Student("Dinca", "Alexandru", a1);
        Student s2 = new Student("Cocu", "Bogdan", a2);
        Student s3 = new Student("Doe", "John", a3);

        studentRepo.save(s1);
        studentRepo.save(s2);
        studentRepo.save(s3);

        Account a4 = new Account("cnicola", "parola");
        Account a5 = new Account("llupu", "parola");

        accountRepo.save(a4);
        accountRepo.save(a5);

        Professor p1 = new Professor("Nicola", "Cornel", a4);
        Professor p2 = new Professor("Lupu", "Lili", a5);
        professorRepo.save(p1);
        professorRepo.save(p2);

        Thesis t1 = new Thesis("JavaApp", s1, p1);
        Thesis t2 = new Thesis("C#App", s2, p1);
        Thesis t3 = new Thesis("PythonApp", null, p2);
        Thesis t4 = new Thesis("CevaApp", null, p2);
        Thesis t5 = new Thesis("EmbeddedSystems", null, p2);
        Thesis t6 = new Thesis("MultimediaApp", null, p1);
        thesisRepo.save(t1);
        thesisRepo.save(t2);
        thesisRepo.save(t3);
        thesisRepo.save(t4);
        thesisRepo.save(t5);
        thesisRepo.save(t6);

        Forum f1 = new Forum("JavaApp Forum", "BLA BLA BLA", p1, s1);
        forumRepo.save(f1);

        t1.setForum(f1);
        thesisRepo.save(t1);

        Forum f2 = new Forum("C#APP Forum", "TEXT TEXT TEXT", p1, s2);
        forumRepo.save(f2);

        t2.setForum(f2);
        thesisRepo.save(t2);

        Comment c1 = new Comment("Buna ziua!", LocalDateTime.now(), s1);
        Comment c2 = new Comment("Asemenea!", LocalDateTime.now(), p1);
        commentRepo.save(c1);
        commentRepo.save(c2);

        f1.addComment(c1);
        f1.addComment(c2);

        forumRepo.save(f1);

        Forum f3 = new Forum("PythonAppForum", "Python is good!", p2, null);
        Forum f4 = new Forum("CevaAppForum", "Hello, this is CevaAppForum description!", p2, null);
        Forum f5 = new Forum("EmbeddedSystemsForum", "EmbeddedSystemsForum description!", p2, null);
        Forum f6 = new Forum("MultimediaAppForum", "MultimediaAppForum description!", p2, null);
        forumRepo.save(f3);
        forumRepo.save(f4);
        forumRepo.save(f5);
        forumRepo.save(f6);

//        Forum forumGlobal = new Forum("Forum Global", "Informatii generale", p1, null);
//        Set<Student> setStudenti = new HashSet<>();
//        setStudenti.add(s1);
//        setStudenti.add(s2);
//        setStudenti.add(s3);
//
//        forumGlobal.setParticipants(setStudenti);
//        forumRepo.save(forumGlobal);



        print();
    }

    private void print() {
        Consumer<Object> loggerConsumer = s -> System.out.println(s);

        studentRepo.findAll().forEach(loggerConsumer);
        professorRepo.findAll().forEach(loggerConsumer);

        commentRepo.findAll().forEach(loggerConsumer);
        forumRepo.findAll().forEach(loggerConsumer);

        thesisRepo.findAll().forEach(loggerConsumer);
    }
}
