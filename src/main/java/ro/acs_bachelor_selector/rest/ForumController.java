package ro.acs_bachelor_selector.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.acs_bachelor_selector.model.Comment;
import ro.acs_bachelor_selector.model.Forum;
import ro.acs_bachelor_selector.repository.ForumRepo;
import ro.acs_bachelor_selector.rest.dto.request.SaveForumRequest;
import ro.acs_bachelor_selector.service.ForumService;
import ro.acs_bachelor_selector.service.exception.ApplicationException;

import java.net.URI;
import java.util.Optional;


@RestController
@RequestMapping("/forums")
public class ForumController {

    @Autowired
    private ForumService forumService;


    /**
     * Find a forum.
     *
     * @param   id
     * @return  status 200 - if the forum is found.
     *          status 404 - if the forum is not found.
     */
    @GetMapping("/{id}")
    public ResponseEntity<Forum> getForum(@PathVariable(value = "id") Integer id) {
        Optional<Forum> byId = forumService.find(id);
        return byId
                .map(forum -> ResponseEntity.ok(forum))
                .orElse(ResponseEntity.notFound().build());
    }

    @GetMapping
    public ResponseEntity<Iterable<Forum>> getAllForums() {
        return ResponseEntity.ok(forumService.findAll());
    }

    /**
     * Add a new forum.
     *
     * Attaches a forum to a thesis.
     *
     * @param   request
     * @return  status 201 - if adding the forum is successful
     *          status 400 - if adding the forum is not successful
     */
    @PostMapping
    public ResponseEntity saveForum(@RequestBody SaveForumRequest request) {
        try {
            final Forum forum = forumService.performSaveForum(request);
            return ResponseEntity.created(URI.create("" + forum.getIdForum())).body(forum);
        } catch (ApplicationException e) {
            return ResponseEntity.badRequest().build();
        }
    }

}