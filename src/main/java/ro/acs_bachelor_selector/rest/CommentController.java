package ro.acs_bachelor_selector.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ro.acs_bachelor_selector.model.Comment;
import ro.acs_bachelor_selector.rest.dto.request.GetFileRequest;
import ro.acs_bachelor_selector.rest.dto.request.SaveCommentRequest;
import ro.acs_bachelor_selector.service.CommentService;
import ro.acs_bachelor_selector.service.exception.ApplicationException;

import java.net.URI;
import java.util.Optional;


@RestController
@RequestMapping("/comments")
public class CommentController {

    private static final Logger LOG = LoggerFactory.getLogger(CommentController.class);

    @Autowired
    private CommentService commentService;


    /**
     * Finds all comments.
     *
     * @return a list of {@link Comment}s with all the comments.
     */
    @GetMapping
    public ResponseEntity<Iterable<Comment>> getAllComments() {
        return ResponseEntity.ok(commentService.findAll());
    }


    /**
     * Finds a comment.
     *
     * @param id
     *
     * @return status 200 - if the comment is found.
     * status 404 - if the comment is not found.
     */
    @GetMapping("/{id}")
    public ResponseEntity<Comment> getComment(@PathVariable(value = "id") Integer id) {
        Optional<Comment> byId = commentService.find(id);
        return byId
                .map(comment -> ResponseEntity.ok(comment))
                .orElse(ResponseEntity.notFound().build());
    }


    /**
     * Add a new comment.
     *
     * @param request
     *
     * @return status 201 - with the id of the comment that was added.
     */
    @PostMapping
    public ResponseEntity addComment(@RequestBody SaveCommentRequest request) {
        try {
            final Comment comment = commentService.processSaveCommentRequest(request);
            return ResponseEntity.created(URI.create("" + comment.getIdComment())).body(comment);
        } catch (ApplicationException e) {
            return ResponseEntity.badRequest().build();
        }
    }


    /**
     * Add a document on a comment.
     *
     * @param commentId
     * @param file
     *
     * @return status 200 - if the save is ok.
     * status 500 - if anything wrong has happened.
     */
    @PostMapping("/{id}/files")
    public ResponseEntity attachFile(@PathVariable("id") Integer commentId, @RequestParam("file") MultipartFile file) {

        try {
            final Comment comment = commentService.attachFile(commentId, file);
            return ResponseEntity.ok(comment);
        } catch (RuntimeException e) {
            LOG.error("error", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }


    /**
     * Get a file from storage.
     *
     * @param request
     *
     * @return a stream of bytes with the file.
     */
    @PostMapping(value = "/files")
    public ResponseEntity serveFile(@RequestBody GetFileRequest request) {
        final ByteArrayResource body = new ByteArrayResource(commentService.getFile(request.getFilename()));
        final Optional<Comment> comment = commentService.find(request.getCommentId());

        if (comment.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity
                .ok()
                .contentType(MediaType.parseMediaType(comment.get().getAttachmentFileType()))
                .body(body);
    }

}