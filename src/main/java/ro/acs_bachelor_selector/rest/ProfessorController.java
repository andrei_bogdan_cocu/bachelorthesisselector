package ro.acs_bachelor_selector.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.acs_bachelor_selector.model.Professor;
import ro.acs_bachelor_selector.model.Student;
import ro.acs_bachelor_selector.repository.ProfessorRepo;
import ro.acs_bachelor_selector.rest.dto.request.LoginPersonRequest;
import ro.acs_bachelor_selector.service.LoginService;
import ro.acs_bachelor_selector.service.exception.AccountNotFoundException;
import ro.acs_bachelor_selector.service.exception.PersonNotFoundException;

import java.util.Optional;


@RestController
@RequestMapping("/professors")
public class ProfessorController {

    @Autowired
    private ProfessorRepo professorRepo;

    @Autowired
    private LoginService loginService;


    /**
     * Find a professor by id.
     *
     * @param   id
     * @return  status 200 - if the professor is found.
     *          status 404 - if the professor is not found.
     */
    @GetMapping("/{id}")
    public ResponseEntity<Professor> getStudent(@PathVariable(value = "id") Integer id) {
        Optional<Professor> byId = professorRepo.findById(id);
        ResponseEntity<Professor> professorResponseEntity = byId.map(professor -> ResponseEntity.ok(professor)).orElse(ResponseEntity.notFound().build());
        return professorResponseEntity;
    }

    /**
     * Login mechanism for logging in professors.
     *
     * @param   request
     * @return  status 200 - if the login is successful
     *          status 401 - if the login is not successful.
     */
    @PostMapping("/login")
    public ResponseEntity<Professor> login(@RequestBody LoginPersonRequest request) {
        try {
            Professor professor = loginService.attemptLoginProfessor(request);
            return ResponseEntity.ok(professor);
        } catch (PersonNotFoundException | AccountNotFoundException e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }
}