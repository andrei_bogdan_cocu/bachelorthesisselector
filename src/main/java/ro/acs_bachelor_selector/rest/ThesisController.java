package ro.acs_bachelor_selector.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.acs_bachelor_selector.model.Thesis;
import ro.acs_bachelor_selector.rest.dto.request.SaveThesisRequest;
import ro.acs_bachelor_selector.rest.dto.request.UpdateThesisRequest;
import ro.acs_bachelor_selector.service.ThesisService;
import ro.acs_bachelor_selector.service.exception.ApplicationException;


import java.net.URI;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/theses")
public class ThesisController {

    @Autowired
    private ThesisService thesisService;


    /**
     * Find all theses.
     *
     * @return  an array of {@link Thesis}
     */
    @GetMapping
    public ResponseEntity<List<Thesis>> getAll() {
        return ResponseEntity.ok(thesisService.findAll());
    }


    /**
     * Find a thesis.
     *
     * @param   id
     * @return  status 200 - if the {@link Thesis} if found
     *          status 404 - if the {@link Thesis} is not found
     */
    @GetMapping("/{id}")
    public ResponseEntity<Thesis> getThesis(@PathVariable(value = "id") Integer id) {
        Optional<Thesis> byId = thesisService.find(id);

        ResponseEntity<Thesis> thesisResponseEntity = byId
                .map(thesis -> ResponseEntity.ok(thesis))
                .orElse(ResponseEntity.notFound().build());

        return thesisResponseEntity;
    }


    /**
     * Add a new thesis.
     *
     * @param   request
     *
     * @return  status 201
     *          The newly created {@link Thesis} object
     */
    @PostMapping
    public ResponseEntity<Thesis> saveThesis(@RequestBody SaveThesisRequest request) {
        try {
            final Thesis thesis = thesisService.processAddThesis(request);
            return ResponseEntity.created(URI.create("" + thesis.getIdThesis())).body(thesis);

        } catch (ApplicationException e) {
            return ResponseEntity.badRequest().build();
        }
    }


    /**
     * Assign a thesis to a student.
     *
     * Updates the thesis by setting the student property to it.
     *
     * @param   id
     * @param   request
     * @return  status 200
     */
    @PutMapping("/{id}")
    public ResponseEntity updateThesis(@PathVariable("id") int id,
                                       @RequestBody UpdateThesisRequest request) {
        try {
            thesisService.processUpdateThesis(id, request);
            return ResponseEntity.ok().build();
        } catch (ApplicationException e) {
            return ResponseEntity.badRequest().build();
        }
    }

}