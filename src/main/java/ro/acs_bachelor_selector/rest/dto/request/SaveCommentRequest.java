package ro.acs_bachelor_selector.rest.dto.request;

public class SaveCommentRequest {

    private Integer forumId;
    private String text;
    private Integer profId;
    private Integer studId;


    public Integer getForumId() {
        return forumId;
    }


    public void setForumId(final Integer forumId) {
        this.forumId = forumId;
    }


    public String getText() {
        return text;
    }


    public void setText(final String text) {
        this.text = text;
    }


    public Integer getProfId() {
        return profId;
    }


    public void setProfId(final Integer profId) {
        this.profId = profId;
    }


    public Integer getStudId() {
        return studId;
    }


    public void setStudId(final Integer studId) {
        this.studId = studId;
    }
}
