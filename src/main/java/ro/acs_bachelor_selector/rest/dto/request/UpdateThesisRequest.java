package ro.acs_bachelor_selector.rest.dto.request;

public class UpdateThesisRequest {

    private int newStudent;


    public int getNewStudent() {
        return newStudent;
    }


    public void setNewStudent(final int newStudent) {
        this.newStudent = newStudent;
    }
}
