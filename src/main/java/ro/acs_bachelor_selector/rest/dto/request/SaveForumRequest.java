package ro.acs_bachelor_selector.rest.dto.request;

public class SaveForumRequest {

    private int thesisId;
    private String name;
    private String text;

    public SaveForumRequest() {
    }


    public String getName() {
        return name;
    }


    public void setName(final String name) {
        this.name = name;
    }


    public String getText() {
        return text;
    }


    public void setText(final String text) {
        this.text = text;
    }


    public int getThesisId() {
        return thesisId;
    }


    public void setThesisId(final int thesisId) {
        this.thesisId = thesisId;
    }
}
