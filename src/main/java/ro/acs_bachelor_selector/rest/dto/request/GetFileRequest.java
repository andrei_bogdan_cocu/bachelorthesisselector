package ro.acs_bachelor_selector.rest.dto.request;

public class GetFileRequest {

    private String filename;
    private int commentId;


    public GetFileRequest() {
    }


    public GetFileRequest(final String filename) {
        this.filename = filename;
    }


    public String getFilename() {
        return filename;
    }


    public void setFilename(final String filename) {
        this.filename = filename;
    }


    public int getCommentId() {
        return commentId;
    }


    public void setCommentId(final int commentId) {
        this.commentId = commentId;
    }
}
