package ro.acs_bachelor_selector.rest.dto.request;

public class SaveThesisRequest {

    private int profId;
    private String title;


    public SaveThesisRequest() {
    }


    public void setProfId(final int profId) {
        this.profId = profId;
    }


    public void setTitle(final String title) {
        this.title = title;
    }


    public int getProfId() {
        return profId;
    }


    public String getTitle() {
        return title;
    }
}
