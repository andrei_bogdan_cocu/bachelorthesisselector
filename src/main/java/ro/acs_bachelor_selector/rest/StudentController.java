package ro.acs_bachelor_selector.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.acs_bachelor_selector.model.Student;
import ro.acs_bachelor_selector.model.Thesis;
import ro.acs_bachelor_selector.rest.dto.request.LoginPersonRequest;
import ro.acs_bachelor_selector.service.LoginService;
import ro.acs_bachelor_selector.service.StudentService;
import ro.acs_bachelor_selector.service.exception.AccountNotFoundException;
import ro.acs_bachelor_selector.service.exception.PersonNotFoundException;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/students")
public class StudentController {

    @Autowired
    private StudentService studentService;

    @Autowired
    private LoginService loginService;


    /**
     * Login mechanism for logging in students.
     *
     * @param   request
     * @return  status 200 - if the login is successful
     *          status 401 - if the login is not successful.
     */
    @PostMapping("/login")
    public ResponseEntity<Student> loginStudent(@RequestBody LoginPersonRequest request) {

        try {
            Student student = loginService.attemptLoginStudent(request);
            return ResponseEntity.ok(student);
        } catch (PersonNotFoundException | AccountNotFoundException e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }


    /**
     * Find a student by id.
     *
     * @param   id
     * @return  status 200 - if the student is found.
     *          status 404 - if the student is not found.
     */
    @GetMapping("/{id}")
    public ResponseEntity<Student> getStudent(@PathVariable(value = "id") Integer id) {
        Optional<Student> byId = studentService.find(id);
        ResponseEntity<Student> studentResponseEntity = byId.map(student -> ResponseEntity.ok(student)).orElse(ResponseEntity.notFound().build());
        return studentResponseEntity;
    }


    @GetMapping
    public ResponseEntity<Iterable<Student>> getAllStudents() {
        return ResponseEntity.ok(studentService.findAll());
    }
}

