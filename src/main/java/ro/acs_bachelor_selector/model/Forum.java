package ro.acs_bachelor_selector.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Entity
@Table(name = "Forums")
public class Forum {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idForum;

    private String forumName;

    private String forumText;

    @OneToOne
    private Professor professor;

    @OneToMany
    private Set<Student> participants = new HashSet<>();

    @OneToMany
    private Set<Comment> comments = new HashSet<>();


    public Forum() {
    }


    public Forum(final String forumName, final String forumText, final Professor professor) {
        this(forumName, forumText, professor, null);
    }


    public Forum(final String name, final String text, final Professor prof, final Student stud) {
        this.forumName = name;
        this.forumText = text;
        this.professor = prof;
        if (stud != null) {
            this.participants.add(stud);
        }
    }


    public Professor getProfessor() {
        return professor;
    }


    public void setProfessor(final Professor professor) {
        this.professor = professor;
    }


    public void addComment(Comment c) {
        this.comments.add(c);
    }


    public Set<Comment> getComments() {
        return comments;
    }


    public void setComments(Set<Comment> comments) {
        this.comments = comments;
    }


    public String getForumText() {
        return forumText;
    }


    public void setForumText(String forumText) {
        this.forumText = forumText;
    }


    public int getIdForum() {
        return idForum;
    }


    public void setIdForum(int idForum) {
        this.idForum = idForum;
    }


    public String getForumName() {
        return forumName;
    }


    public void setForumName(String forumName) {
        this.forumName = forumName;
    }


    public Set<Student> getParticipants() {
        return participants;
    }


    public void setParticipants(final Set<Student> participants) {
        this.participants = participants;
    }

    public void addParticipant(Student s)
    {
        participants.add(s);
    }


    @Override
    public String toString() {
        return "Forum{" +
                "idForum=" + idForum +
                ", forumName='" + forumName + '\'' +
                ", forumText='" + forumText + '\'' +
                ", comments=" + comments +
                '}';
    }
}
