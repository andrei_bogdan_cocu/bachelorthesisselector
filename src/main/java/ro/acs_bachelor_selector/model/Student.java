package ro.acs_bachelor_selector.model;

import javax.persistence.*;


@Entity
@Table(name = "Students")
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idStudent;

    private String lastName;
    private String firstName;

    @OneToOne
    private Account account;


    public Student() {
    }


    public Student(final String lastName, final String firstName, final Account account) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.account = account;
    }


    public String getLastName() {
        return lastName;
    }


    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


    public Account getAccount() {
        return account;
    }


    public void setAccount(final Account account) {
        this.account = account;
    }


    public String getFirstName() {
        return firstName;
    }


    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }


    public int getIdStudent() {
        return idStudent;
    }


    public void setIdStudent(int idStudent) {
        this.idStudent = idStudent;
    }


    @Override
    public String toString() {
        return "Student{" +
                "lastName='" + lastName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", idStudent=" + idStudent +
                '}';
    }
}