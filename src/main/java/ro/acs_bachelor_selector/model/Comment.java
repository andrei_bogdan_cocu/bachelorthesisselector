package ro.acs_bachelor_selector.model;

import javax.persistence.*;
import java.time.LocalDateTime;


@Entity
@Table(name = "Comments")
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idComment;

    private String commentText;

    private LocalDateTime timestamp;

    @OneToOne
    private Student studentCommenter;

    @OneToOne
    private Professor professorCommenter;

    private String attachment;
    private String attachmentFileType;


    public Comment() {
    }



    public Comment(String commentText, LocalDateTime timestamp, Student studentCommenter) {
        this.commentText = commentText;
        this.timestamp = timestamp;
        this.studentCommenter = studentCommenter;
    }


    public Comment(String commentText, LocalDateTime timestamp, Professor professorCommenter) {
        this.commentText = commentText;
        this.timestamp = timestamp;
        this.professorCommenter = professorCommenter;
    }


    public int getIdComment() {
        return idComment;
    }


    public void setIdComment(int idComment) {
        this.idComment = idComment;
    }


    public LocalDateTime getTimestamp() {
        return timestamp;
    }


    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }


    public String getCommentText() {
        return commentText;
    }


    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }


    public Student getStudentCommenter() {
        return studentCommenter;
    }


    public void setStudentCommenter(Student studentCommenter) {
        this.studentCommenter = studentCommenter;
    }


    public Professor getProfessorCommenter() {
        return professorCommenter;
    }


    public void setProfessorCommenter(Professor professorCommenter) {
        this.professorCommenter = professorCommenter;
    }


    public String getAttachment() {
        return attachment;
    }


    public void setAttachment(final String attachment) {
        this.attachment = attachment;
    }


    public String getAttachmentFileType() {
        return attachmentFileType;
    }


    public void setAttachmentFileType(final String attachmentFileType) {
        this.attachmentFileType = attachmentFileType;
    }


    @Override
    public String toString() {
        return "Comment{" +
                "idComment=" + idComment +
                ", commentText='" + commentText + '\'' +
                ", timestamp=" + timestamp +
                ", studentCommenter=" + studentCommenter +
                ", professorCommenter=" + professorCommenter +
                '}';
    }
}
