package ro.acs_bachelor_selector.model;

import javax.persistence.*;
import java.util.Objects;


@Entity
@Table(name="Professors")
public class Professor
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idProfessor;

    private String lastName;

    private String firstName;

    @OneToOne
    private Account account;


    public Professor() {
    }

    public Professor(String lastName, String firstName, final Account a4) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.account = a4;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public int getIdProfessor() {
        return idProfessor;
    }

    public void setIdProfessor(int idProfessor) {
        this.idProfessor = idProfessor;
    }


    public Account getAccount() {
        return account;
    }


    public void setAccount(final Account account) {
        this.account = account;
    }


    @Override
    public String toString() {
        return "Professor{" +
                "lastName='" + lastName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", idProfessor=" + idProfessor +
                '}';
    }


    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Professor professor = (Professor) o;
        return idProfessor == professor.idProfessor &&
                Objects.equals(lastName, professor.lastName) &&
                Objects.equals(firstName, professor.firstName);
    }


    @Override
    public int hashCode() {
        return Objects.hash(idProfessor, lastName, firstName);
    }
}
