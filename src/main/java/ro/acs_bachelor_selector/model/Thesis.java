package ro.acs_bachelor_selector.model;

import javax.persistence.*;

import static java.lang.Boolean.FALSE;


@Entity
@Table(name = "Thesis")
public class Thesis {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idThesis;

    @OneToOne
    private Student stud;

    @OneToOne
    private Professor prof;

    private String title;

    @OneToOne
    private Forum forum;

    private boolean taken = FALSE;

    public boolean isTaken() {
        return taken;
    }

    public void setTaken(boolean taken) {
        this.taken = taken;
    }

    public Thesis() {
    }


    public Thesis(String title, Student stud, Professor prof) {
        this.title = title;
        this.stud = stud;
        this.prof = prof;
        if(stud != null)
        setTaken(true);
    }


    public Forum getForum() {
        return forum;
    }


    public void setForum(final Forum forum) {
        this.forum = forum;
    }


    public int getIdThesis() {
        return idThesis;
    }


    public void setIdThesis(int idThesis) {
        this.idThesis = idThesis;
    }


    public Student getStud() {
        return stud;
    }


    public void setStud(Student stud) {
        this.stud = stud;
    }


    public Professor getProf() {
        return prof;
    }


    public void setProf(Professor prof) {
        this.prof = prof;
    }


    public String getTitle() {
        return title;
    }


    public void setTitle(String thesisName) {
        this.title = thesisName;
    }


    @Override
    public String toString() {
        return "Thesis{" +
                "idThesis=" + idThesis +
                ", stud=" + stud +
                ", prof=" + prof +
                ", title='" + title + '\'' +
                '}';
    }
}
