package ro.acs_bachelor_selector.repository;

import org.springframework.data.repository.CrudRepository;
import ro.acs_bachelor_selector.model.Professor;

import java.util.Optional;


public interface ProfessorRepo extends CrudRepository<Professor, Integer> {

    Optional<Professor> findByAccount_Id(Integer id);
}
