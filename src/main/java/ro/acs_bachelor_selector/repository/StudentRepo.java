package ro.acs_bachelor_selector.repository;

import org.springframework.data.repository.CrudRepository;
import ro.acs_bachelor_selector.model.Student;

import java.util.Optional;


public interface StudentRepo extends CrudRepository<Student, Integer> {

    Optional<Student> findByAccount_Id(Integer accountId);

}
