package ro.acs_bachelor_selector.repository;

import org.springframework.data.repository.CrudRepository;
import ro.acs_bachelor_selector.model.Thesis;

public interface ThesisRepo extends CrudRepository<Thesis, Integer> {
}
