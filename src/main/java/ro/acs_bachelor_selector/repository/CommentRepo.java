package ro.acs_bachelor_selector.repository;

import org.springframework.data.repository.CrudRepository;
import ro.acs_bachelor_selector.model.Comment;

public interface CommentRepo extends CrudRepository<Comment, Integer> {
}
