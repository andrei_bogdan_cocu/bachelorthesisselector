package ro.acs_bachelor_selector.repository;

import org.springframework.data.repository.CrudRepository;
import ro.acs_bachelor_selector.model.Forum;

public interface ForumRepo extends CrudRepository<Forum, Integer> {
}
