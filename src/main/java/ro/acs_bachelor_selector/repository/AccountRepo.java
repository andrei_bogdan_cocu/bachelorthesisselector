package ro.acs_bachelor_selector.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ro.acs_bachelor_selector.model.Account;

import java.util.Optional;


@Repository
public interface AccountRepo extends CrudRepository<Account, Integer> {

    Optional<Account> findAccountByUsernameAndPassword(String username, String password);

}
